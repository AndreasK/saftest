/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/vorbis/doc/framing.html
// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://www.ietf.org/rfc/rfc3533.txt

// Note that Ogg is a container format encapsulating e.g. Vorbis-encoded audio and Vorbis comments.


import android.util.Log;
import java.io.InputStream;

import static java.lang.System.arraycopy;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SameParameterValue"})
public class TagsOggVorbis extends TagsVorbis
{
    private static final String LOG_TAG = "TOGG";
    int expectedPageNo = -1;            // pre-increment
    private byte[] data = null;         // packet data, combined from pages


    private class OggPage
    {
        private int noOfSegments = 0;
        int pageSize = 0;           // sum of all segment sizes
        boolean lastPage = false;           // a segment < 255 marks the last page
        int pageSeqNo;

        private final byte[] data = new byte[27];
        @SuppressWarnings("FieldCanBeLocal")
        private byte[] lacing = null;

        public boolean read()
        {
            expectedPageNo++;
            return readBytes(data);
        }

        public boolean readLacingTable()
        {
            pageSize = 0;
            if (noOfSegments > 0)
            {
                lacing = new byte[noOfSegments];
                if (!readBytes(lacing))
                {
                    return false;
                }
                for (byte b : lacing)
                {
                    int segmentSize = (b & 0xff);
                    pageSize += segmentSize;
                    if (segmentSize < 255)
                    {
                        Log.d(LOG_TAG, "OggPage::readLacingTable() : segmentSize = " + segmentSize + ", this is the last page");
                        lastPage = true;
                    }
                }
            }
            Log.d(LOG_TAG, "OggPage::readLacingTable() : page size = " + pageSize);
            return true;
        }

        public boolean check()
        {
            if ((data[0] != 'O') || (data[1] != 'g') || (data[2] != 'g') || (data[3] != 'S'))
            {
                Log.e(LOG_TAG, "OggPage::check() : missing OggS signature");
                return false;
            }

            if (data[4] != 0)
            {
                Log.e(LOG_TAG, "OggPage::check() : structure revision must be 0");
                return false;
            }

            Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 1) != 0) ? "continued" : "fresh"));
            Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 2) != 0) ? "first" : "not first"));
            Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 4) != 0) ? "last" : "not last"));
            if ((data[5] & 0xf8) != 0)
            {
                Log.w(LOG_TAG, "OggPage::check() : additional flags found.");
            }

            // this has something to do with the timestamp, so we do not care
            long absGranPos = getLittleEndianInt8(data, 6);
            Log.d(LOG_TAG, "OggPage::check() : absolute granule position = " + absGranPos);

            int streamSerialNo = getLittleEndianInt4(data, 14);
            Log.d(LOG_TAG, "OggPage::check() : stream serial number = " + streamSerialNo);

            pageSeqNo = getLittleEndianInt4(data, 18);
            Log.d(LOG_TAG, "OggPage::check() : page sequence number = " + pageSeqNo);

            int pageCheckSum = getLittleEndianInt4(data, 22);
            Log.d(LOG_TAG, "OggPage::check() : page checksum = " + pageCheckSum);

            noOfSegments = data[26] & 0xff;     // make sure this is unsigned!
            Log.d(LOG_TAG, "OggPage::check() : number of segments = " + noOfSegments);

            return true;
        }

        public boolean isContinued()
        {
            return ((data[5] & 1) != 0);
        }
    }


    private class Vorbis
    {
        @SuppressWarnings("FieldCanBeLocal")
        int packetType;
        int offset = 0;         // offset in packet data

        public boolean check()
        {
            packetType = data[0] & 0xff;
            Log.d(LOG_TAG, "Vorbis::check() : packet type is " + packetType);

            if ((data[1] != 'v') || (data[2] != 'o') || (data[3] != 'r') || (data[4] != 'b') || (data[5] != 'i') || (data[6] != 's'))
            {
                Log.e(LOG_TAG, "Vorbis::check() : missing vorbis signature");
                return false;
            }

            offset = 7;
            return true;
        }

        boolean getIdentificationHeader()
        {
            int version = getLittleEndianInt4(data, offset);
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : version = " + version);

            int audioChannels = data[offset + 4];
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : number of audio channels = " + audioChannels);

            int audioSampleRate = getLittleEndianInt4(data, offset + 5);
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + audioSampleRate);

            int bitrateMaximum = getLittleEndianInt4(data, offset + 9);
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : maximum bitrate = " + bitrateMaximum);
            int bitrateNominal = getLittleEndianInt4(data, offset + 13);
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + bitrateNominal);
            int bitrateMinimum = getLittleEndianInt4(data, offset + 17);
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + bitrateMinimum);

            int blocksize = data[offset + 21] & 0xff;
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : blocksize0 = " + (blocksize & 0x0f));
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : blocksize1 = " + (blocksize >> 4));

            int framing = data[offset + 22];
            Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : framing = " + framing);
            offset += 23;

            return true;
        }

        boolean getComment()
        {
            InputBuffer buf = new InputBuffer(data, offset);
            //noinspection RedundantIfStatement
            if (!readVorbisComment(buf))
            {
                return false;
            }

            return true;
        }
    }

    public TagsOggVorbis(InputStream is)
    {
        mInputStream = is;
        tagType = 11;            // Vorbis
    }


    // combine Ogg pages to a packet. Each page is about 64 k maximum.
    private boolean readPacket()
    {
        int i;
        final int maxPages = 1024;      // about 64 Megabytes
        byte[][] pageData = new byte[maxPages][];
        int packetSize = 0;

        for (i = 0; i < maxPages; i++)
        {
            bytesLeft = Long.MAX_VALUE;
            OggPage page = new OggPage();
            if (!page.read())
            {
                return false;
            }
            if (!page.check())
            {
                return false;
            }
            if (!page.readLacingTable())
            {
                return false;
            }
            if (expectedPageNo != page.pageSeqNo)
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : wrong page sequence number " + page.pageSeqNo);
                return false;
            }
            if ((i == 0) && page.isContinued())
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : first page cannot be continued");
                return false;
            }
            else
            if ((i > 0) && !page.isContinued())
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : first page cannot be continued");
                return false;
            }

            // each page gets its buffer
            pageData[i] = new byte[page.pageSize];
            if (!readBytes(pageData[i]))
            {
                return false;
            }
            packetSize += page.pageSize;

            if (page.lastPage)
            {
                break;
            }
        }

        if (i < maxPages)
        {
            // copy all pages to common buffer
            data = new byte[packetSize];
            int destPos = 0;
            for (int j = 0; j <= i; j++)
            {
                int pageLength = pageData[j].length;
                arraycopy(pageData[j], 0, data, destPos, pageLength);
                destPos += pageLength;
            }
            Log.d(LOG_TAG, "Vorbis::readPacket() : packet has " + packetSize + " bytes in " + (i + 1) + " pages.");
        }
        else
        {
            Log.e(LOG_TAG, "Vorbis::readPacket() : more than " + i + " pages.");
            return false;
        }

        return true;
    }

    public boolean read()
    {
        // read Ogg packet, consisting of pages, to memory

        if (!readPacket())
        {
            return false;
        }

        //
        // read first Vorbis packet inside the Ogg packet, must have type 1
        //

        Vorbis vorbis = new Vorbis();
        if (!vorbis.check())
        {
            return false;
        }
        if (vorbis.packetType == 1)
        {
            if (!vorbis.getIdentificationHeader())
            {
                return false;
            }
        }
        else
        {
            Log.e(LOG_TAG, "TagsVorbis::read() : unhandled packet type " + vorbis.packetType);
            return false;
        }

        //
        // read second Ogg packet
        //

        if (!readPacket())
        {
            return false;
        }

        //
        // read second Vorbis packet inside the Ogg packet, must have type 3
        //

        vorbis = new Vorbis();
        if (!vorbis.check())
        {
            return false;
        }
        if (vorbis.packetType == 3)
        {
            //noinspection RedundantIfStatement
            if (!vorbis.getComment())
            {
                return false;
            }
        }
        else
        {
            Log.e(LOG_TAG, "TagsVorbis::read() : unhandled packet type " + vorbis.packetType);
            return false;
        }

        return true;
    }
}
