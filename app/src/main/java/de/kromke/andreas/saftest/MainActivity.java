package de.kromke.andreas.saftest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.audiotags.TagsBase;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class MainActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "SafTest";
    protected static final int REQUEST_DOCUMENT_SELECT = 4;
    protected static final int REQUEST_DIRECTORY_SELECT = 5;
    protected static final int REQUEST_DOCUMENT_CREATE = 6;
    protected static final int REQUEST_DOCUMENT_DELETE = 7;
    final private static boolean msPersistablePermissions = false;
    final private static boolean msWriteTest = false;
    final private static boolean msTagReader = true;
    private TextView mText;
    @SuppressWarnings("FieldCanBeLocal")
    private Uri mCreatedDocumentUri = null;


    // Used to load the 'native-lib' library on application startup.
    static
    {
        System.loadLibrary("native-lib");
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        mText = findViewById(R.id.sample_text);
        mText.setText(stringFromJNI());
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        Log.d(LOG_TAG, "onCreateOptionsMenu()");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


    /**************************************************************************
     *
     * menu handling
     *
     *************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.d(LOG_TAG, "onOptionsItemSelected()");
        switch (item.getItemId())
        {
            case R.id.action_open_document:
                startSafPickerForOpenDocument();
                break;

            case R.id.action_open_document_tree:
                startSafPickerForTree();
                break;

            case R.id.action_create_document:
                startSafPickerForCreateDocument();
                break;

            case R.id.action_delete_document:
                startSafPickerForDeleteDocument();
                break;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
        return true;
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();


    /**************************************************************************
     *
     * helper to start SAF file selector
     *
     *************************************************************************/
    protected void startSafPickerForTree()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        // specify initial directory tree position
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            if (mDir != null)
            {
                String uriString = mDir.getCurrentUriAsString();
                if (uriString != null)
                {
                    Uri uri = Uri.parse(uriString); // "content://com.android.externalstorage.documents/document/primary%3AMusic"
                    intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
                }
            }
        }
        */

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        int flags = Intent.FLAG_GRANT_READ_URI_PERMISSION + Intent.FLAG_GRANT_WRITE_URI_PERMISSION + Intent.FLAG_GRANT_PREFIX_URI_PERMISSION;
        if (msPersistablePermissions)
        {
            flags += Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
        }
        intent.addFlags(flags);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        startActivityForResult(intent, REQUEST_DIRECTORY_SELECT);
    }


    /**************************************************************************
     *
     * helper to start SAF file selector
     *
     *************************************************************************/
    protected void startSafPickerForOpenDocument()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // specify initial directory tree position
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            if (mDir != null)
            {
                String uriString = mDir.getCurrentUriAsString();
                if (uriString != null)
                {
                    Uri uri = Uri.parse(uriString); // "content://com.android.externalstorage.documents/document/primary%3AMusic"
                    intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
                }
            }
        }
        */
        // Ask for read and write access to file.
        int flags = Intent.FLAG_GRANT_READ_URI_PERMISSION + Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        if (msPersistablePermissions)
        {
            flags += Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
        }
        intent.addFlags(flags);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_DOCUMENT_SELECT);
    }


    /**************************************************************************
     *
     * helper to start SAF file selector
     *
     *************************************************************************/
    protected void startSafPickerForCreateDocument()
    {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.putExtra(Intent.EXTRA_TITLE, "Blibla");
        //  DocumentsContract#EXTRA_INITIAL_URI
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_DOCUMENT_CREATE);
    }


    /**************************************************************************
     *
     * helper to start SAF file selector
     *
     *************************************************************************/
    protected void startSafPickerForDeleteDocument()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.setType("*/*");
        startActivityForResult(intent, REQUEST_DOCUMENT_DELETE);
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * This is called when a secondary activity has ended.
     *
     *************************************************************************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData)
    {
        Log.d(LOG_TAG, "onActivityResult(req = " + requestCode + ", res = " + resultCode + ")");

        switch (requestCode)
        {
            case REQUEST_DOCUMENT_SELECT:
                if (resultCode == RESULT_OK)
                {
                    Uri docUri = resultData.getData();
                    if (docUri != null)
                    {
                        Log.d(LOG_TAG, "Uri from Document Selector: " + docUri);
                        Log.d(LOG_TAG, "               and as path: " + docUri.getPath());

                        if (msPersistablePermissions)
                        {
                            grantUriPermission(getPackageName(), docUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(docUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }

                        if (msTagReader)
                        {
                            readTagsFromUri(docUri);
                        }
                        else
                        {
                            DocumentFile df = DocumentFile.fromSingleUri(this, docUri);
                            if (df == null)
                            {
                                Log.e(LOG_TAG, "onActivityResult() : cannot get DocumentFile");
                            }
                            else
                            {
                                testDocumentFile(df);
                            }
                        }
                    }
                }
                break;

            case REQUEST_DOCUMENT_CREATE:
                if (resultCode == RESULT_OK)
                {
                    mCreatedDocumentUri = resultData.getData();
                    if (mCreatedDocumentUri != null)
                    {
                        Log.d(LOG_TAG, "Uri from Document Create Selector: " + mCreatedDocumentUri);
                        Log.d(LOG_TAG, "               and as path: " + mCreatedDocumentUri.getPath());
                    }
                }
                break;

            case REQUEST_DOCUMENT_DELETE:
                if (resultCode == RESULT_OK)
                {
                    Uri docUri = resultData.getData();
                    if (docUri != null)
                    {
                        Log.d(LOG_TAG, "Uri from Document Delete Selector: " + docUri);
                        Log.d(LOG_TAG, "               and as path: " + docUri.getPath());
                        try
                        {
                            int nrows = getContentResolver().delete(docUri,null ,null);
                            Log.d(LOG_TAG, "               number of rows deleted: " + nrows);
                        } catch (Exception e)
                        {
                            Log.e(LOG_TAG, "fromDocumentFile() : cannot delete document, exception: " + e);
                        }
                    }
                }
                break;

            case REQUEST_DIRECTORY_SELECT:
                if (resultCode == RESULT_OK)
                {
                    Uri treeUri = resultData.getData();
                    if (treeUri != null)
                    {
                        Log.d(LOG_TAG, "Uri from Document Selector: " + treeUri);
                        Log.d(LOG_TAG, "               and as path: " + treeUri.getPath());

                        if (msPersistablePermissions)
                        {
                            grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        }

                        DocumentFile df = DocumentFile.fromTreeUri(this, treeUri);
                        if (df == null)
                        {
                            Log.e(LOG_TAG, "onActivityResult() : cannot get DocumentFile tree");
                        }
                    }
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, resultData);
                break;
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private String toString(final DocumentFile df)
    {
        return (df == null) ? "null" : df.toString();
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings({"unused", "RedundantSuppression"})
    private String toUriString(final DocumentFile df)
    {
        return (df == null) ? "null" : df.getUri().toString();
    }


    /**************************************************************************
     *
     * main test routine
     *
     * https://developer.android.com/reference/android/support/v4/provider/DocumentFile
     *
     *************************************************************************/
    private void testDocumentFile(final DocumentFile df)
    {
        if (df == null)
        {
            return;
        }
        Log.d(LOG_TAG, "Uri = \"" + df.getUri() + "\"");
        Log.d(LOG_TAG, "Name = \"" + df.getName() + "\"");
        Log.d(LOG_TAG, "Type = \"" + df.getType() + "\"");
        Log.d(LOG_TAG, "canRead = " + df.canRead());
        Log.d(LOG_TAG, "canWrite = " + df.canWrite());
        Log.d(LOG_TAG, "isDirectory = " + df.isDirectory());
        Log.d(LOG_TAG, "isDocumentUri = " + DocumentFile.isDocumentUri(this, df.getUri()));
        Log.d(LOG_TAG, "isFile = " + df.isFile());
        Log.d(LOG_TAG, "isVirtual = " + df.isVirtual());
        Log.d(LOG_TAG, "Parent = " + df.getParentFile());
        Log.d(LOG_TAG, "lastModified = " + df.lastModified());
        Log.d(LOG_TAG, "length = " + df.length());
        testDocUri(df.getUri());
    }


    /**************************************************************************
     *
     * main test routine
     *
     * https://developer.android.com/reference/android/net/Uri.html
     *
     *************************************************************************/
    private void testDocUri(final Uri uri)
    {
        if (uri == null)
        {
            return;
        }
        Log.d(LOG_TAG, "Uri Authority = \"" + uri.getAuthority() + "\"");
        Log.d(LOG_TAG, "Uri encoded Authority = \"" + uri.getEncodedAuthority() + "\"");
        Log.d(LOG_TAG, "Uri Path = \"" + uri.getPath() + "\"");
        Log.d(LOG_TAG, "Uri encoded Path = \"" + uri.getEncodedPath() + "\"");
        Log.d(LOG_TAG, "Uri Fragment = \"" + uri.getFragment() + "\"");
        Log.d(LOG_TAG, "Uri encoded Fragment = \"" + uri.getEncodedFragment() + "\"");
        Log.d(LOG_TAG, "Uri encoded UserInfo = \"" + uri.getEncodedUserInfo() + "\"");
        Log.d(LOG_TAG, "Uri Query = \"" + uri.getQuery() + "\"");
        Log.d(LOG_TAG, "Uri encoded Query = \"" + uri.getEncodedQuery() + "\"");
        Log.d(LOG_TAG, "Uri Scheme = \"" + uri.getScheme() + "\"");
        Log.d(LOG_TAG, "Uri encoded SchemeSpecificPart = \"" + uri.getEncodedSchemeSpecificPart() + "\"");
        Log.d(LOG_TAG, "Uri Host = \"" + uri.getHost() + "\"");
        Log.d(LOG_TAG, "Uri Port = " + uri.getPort());
        Log.d(LOG_TAG, "Uri isAbsolute = " + uri.isAbsolute());
        Log.d(LOG_TAG, "Uri isRelative = " + uri.isRelative());
        Log.d(LOG_TAG, "Uri isHierarchical = " + uri.isHierarchical());
        Log.d(LOG_TAG, "Uri isOpaque = " + uri.isOpaque());

        // https://developer.android.com/reference/android/content/ContentResolver#openInputStream(android.net.Uri)
        ContentResolver cr = getContentResolver();
        if (cr != null)
        {
            InputStream is = null;
            OutputStream osw = null;
            OutputStream osrw = null;
            ParcelFileDescriptor fdr = null;
            ParcelFileDescriptor fdw = null;
            ParcelFileDescriptor fdrw = null;

            try
            {
                is = cr.openInputStream(uri);
                fdr = cr.openFileDescriptor(uri, "r");
                if (msWriteTest)
                {
                    osw = cr.openOutputStream(uri, "w");
                    fdw = cr.openFileDescriptor(uri, "w");
                    osrw = cr.openOutputStream(uri, "rw");
                    fdrw = cr.openFileDescriptor(uri, "rw");
                }
            } catch (FileNotFoundException | UnsupportedOperationException e)
            {
                e.printStackTrace();
            }

            testInputOutput(is, osw, osrw, fdr, fdw, fdrw);
        }
    }


    /**************************************************************************
     *
     * main test routine
     *
     * https://developer.android.com/reference/android/net/Uri.html
     * https://developer.android.com/reference/android/os/ParcelFileDescriptor
     * https://developer.android.com/reference/java/io/FileInputStream
     * https://developer.android.com/reference/java/io/FileOutputStream
     * https://developer.android.com/reference/java/nio/channels/FileChannel
     *
     *************************************************************************/
    private void testInputOutput(
            final InputStream is,
            final OutputStream osw,
            final OutputStream osrw,
            final ParcelFileDescriptor fdr,
            final ParcelFileDescriptor fdw,
            final ParcelFileDescriptor fdrw)
    {
        ByteBuffer buf = ByteBuffer.allocate(1024);

        Log.d(LOG_TAG, "Uri InputStream = \"" + is + "\"");
        Log.d(LOG_TAG, "Uri OutputStream w = \"" + osw + "\"");
        Log.d(LOG_TAG, "Uri OutputStream rw = \"" + osrw + "\"");
        Log.d(LOG_TAG, "Uri ParcelFileDescriptor r = \"" + fdr + "\"");
        Log.d(LOG_TAG, "Uri ParcelFileDescriptor w = \"" + fdw + "\"");
        Log.d(LOG_TAG, "Uri ParcelFileDescriptor rw = \"" + fdrw + "\"");

        // read test
        if (fdr != null)
        {
            Log.d(LOG_TAG, "Uri ParcelFileDescriptor r StatSize = " + fdr.getStatSize());
            Log.d(LOG_TAG, "Uri ParcelFileDescriptor r fd = " + fdr.getFd());
            Log.d(LOG_TAG, "Uri ParcelFileDescriptor r fileDescriptor = " + fdr.getFileDescriptor());

            FileInputStream fis = new FileInputStream(fdr.getFileDescriptor());
            FileChannel ifc = fis.getChannel();

            Log.d(LOG_TAG, "Uri ParcelFileDescriptor r FileInputStream = " + fis);
            Log.d(LOG_TAG, "Uri ParcelFileDescriptor r FileChannel = " + ifc);

            // read 42 bytes from position 13 of the file
            int result;
            try
            {
                result = ifc.read(buf, 13);
            } catch (IOException e)
            {
                result = -1;
                Log.e(LOG_TAG, "FileChannel random access read exception: " + e);
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "FileChannel random access read result: " + result);
        }
    }


    /**************************************************************************
     *
     * main test routine
     *
     * https://developer.android.com/reference/android/support/v4/provider/DocumentFile
     *
     *************************************************************************/
    private void readTagsFromUri(final Uri theUri)
    {
        if (theUri == null)
        {
            return;
        }

        // cover picture callback
        TagsBase.TagCallbacks cb = new TagsBase.TagCallbacks()
        {
            public int handlePicture(byte[] data, int index, int length, int pictureType, final String mimeType)
            {
                Log.d(LOG_TAG, "readTagsFromUri.cb() : found picture of type=" + pictureType + ", MIME type=" + mimeType + ", length=" + length);
                return 0;
            }
        };

        /*
        // cover picture
        OutputStream outputStream = null;
        if (mCreatedDocumentUri != null)
        {
            try
            {
                // we need mode "wt", but this is not possible, so we use mode "w"
                outputStream = getContentResolver().openOutputStream(mCreatedDocumentUri);
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "fromDocumentFile() : cannot open output stream, exception: " + e);
            }
        }
        */

        boolean res;
        TagsBase tags = TagsBase.fromUri(theUri, this);
        if (tags != null)
        {
            tags.setPictureHandler(cb);
            res = tags.read();
            if (res)
            {
                tags.dump();
                mText.setText(tags.getDump());
                /*
                if ((outputStream != null) && (tags.coverPictureSize > 0))
                {
                    final String ext = (tags.coverPictureType == 1) ? "jpg" : "png";
                    try
                    {
                        DocumentsContract.renameDocument(getContentResolver(), mCreatedDocumentUri, "coverpicture." + ext);
                    } catch (FileNotFoundException e)
                    {
                        Log.e(LOG_TAG, "fromDocumentFile() : cannot rename document, exception: " + e);
                    }
                }
                */
            }
        }
        else
        {
            res = false;
        }

        Log.d(LOG_TAG, "readTagsFromUri() : result =  " + (res ? "success" : "failure"));
    }
}
